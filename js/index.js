// Global app controller
"use strict"
const config = {
    key: 'fa69a3beb9e2b10840d64dcfd521b6aa',
    // proxy: 'https://cors-anywhere.herokuapp.com/',
    proxy: 'https://cors.io/?',
    api: 'http://food2fork.com/api/'
};

// Все элементы
const elements = {
    searchForm: document.querySelector('.search'),
    searchInput: document.querySelector('.search__field'),
    searchRes: document.querySelector('.results'),
    searchResList: document.querySelector('.results__list'),
    searchResPages: document.querySelector('.results__pages'),
    recipe: document.querySelector('.recipe')
};

// Loader
const loader = {
  render(parent) {
      const loader = `
        <div class="loader">
            <svg>
                <use href="img/icons.svg#icon-cw"></use>
            </svg>
        </div>
      `;

      parent.insertAdjacentHTML("afterbegin", loader);
  },
  clear() {
    const loader = document.querySelector('.loader');
    if (loader) loader.parentElement.removeChild(loader);
  }
};

// Search model
const searchModel = {
    result: [],
    query: '',
    getResults(value, callback) {
        // this = searchModel;
        this.query = value;
        http.get(`${config.proxy}${config.api}search?key=${config.key}&q=${value}`, function (error, res) {
            searchModel.result = JSON.parse(res);
            callback(JSON.parse(res));
        })
    }
};

// Search view
const searhView = {
    getInput() {
        return elements.searchInput.value;
    },
    clearInput() {
        elements.searchForm.reset();
    },
    clearResults() {
        elements.searchResList.innerHTML = '';
        elements.searchResPages.innerHTML = '';
    },
    renderRecipe(recipe) {
        console.log(recipe);
        // создать маркап под один рецепт
        const markup = `
            <li data-id="${recipe.recipe_id}">
                <a class="results__link" href="#${recipe.recipe_id}">
                    <figure class="results__fig">
                        <img src="${recipe.image_url}" alt="${recipe.title}">
                    </figure>
                    <div class="results__data">
                        <h4 class="results__name">${recipe.title}</h4>
                        <p class="results__author">${recipe.publisher}</p>
                    </div>
                </a>
            </li>
        `;
        // добавить в разметку
        elements.searchResList.insertAdjacentHTML("beforeend", markup);
    },
    createButton(page, type) {
        const button = `
            <button class="btn-inline results__btn--${type}" data-goto="${type === 'prev' ? page - 1 : page + 1}">                
                <span>Page ${type === 'prev' ? page - 1 : page + 1}</span>

                <svg class="search__icon">
                    <use href="img/icons.svg#icon-triangle-${type === 'prev' ? 'left' : 'right'}"></use>
                </svg>
            </button>
        `;

        return button;
    },
    renderButtons(page, numResults, resPerPage) {
        const pages = Math.ceil(numResults / resPerPage);
        let button;

        if (page === 1 && pages > 1) {
            // вывести кнопку ->
            button = this.createButton(page, 'next');
        } else if (page < pages) {
            // вывести обе кнопки
            button = `
                ${this.createButton(page, 'prev')}
                ${this.createButton(page, 'next')}
            `;
        } else if (page === pages && pages > 1) {
            // вывести prev <-
            button = this.createButton(page, 'prev');
        }

        if (button) {
            elements.searchResPages.insertAdjacentHTML("afterbegin", button);
        }
    },
    renderResult(recipes, page = 1, recipePerPage = 10) {
        const start = (page - 1) * recipePerPage;
        const end = page * recipePerPage;
        const recipesToRender = recipes.slice(start, end);

        for (let recipe of recipesToRender) {
            // this = searhView
            // передаем по одному рецепту в функцию renderRecipe
            this.renderRecipe(recipe);
        }

        // вывести кнопки
        this.renderButtons(page, recipes.length, recipePerPage);
    }
};

// Search controller
function searchController() {
    const query = searhView.getInput();

    if (query) {
        // очистим форму
        searhView.clearInput();
        // очистили разметку
        searhView.clearResults();
        // показать loader
        loader.render(elements.searchRes);
        //делвем запрос на получение рецептов
       searchModel.getResults(query, function (res) {
           // убираем loader
           loader.clear();
           if (res.count) {
               // Рендерим результаты
               searhView.renderResult(res.recipes);
           } else {
               alert("Recipes not found!");
           }
       });
    }
}

elements.searchForm.addEventListener('submit', function (e) {
   e.preventDefault();
   searchController();
});

elements.searchResPages.addEventListener('click', function (e) {
   const btn = e.target.closest('.btn-inline');

   if (btn) {
       const goToPage = parseInt(btn.dataset.goto);
       searhView.clearResults();
       searhView.renderResult(searchModel.result.recipes, goToPage);
   }
});


// Recipe model
const recipeModel = {
    id : '',
    result: {},
    getRecipe(id, callback) {
       this.id = id;
       http.get(`${config.proxy}${config.api}get?key=${config.key}&rId=${id}`, function (error, res) {
           recipeModel.result = JSON.parse(res);
           callback(JSON.parse(res));
       });
    }
};

const recipeView = {
    clearRecipe() {
        elements.recipe.innerHTML = '';
    },
    getRecipeId(target) {
        const li = target.closest('li');
        if (li) {
            return li.dataset.id;
        }
    },
    createIngredient(ingredients) {
        let markup = '';

        for (let ingredient of ingredients) {
            markup += `
                <li class="recipe__item">
                    <svg class="recipe__icon">
                        <use href="img/icons.svg#icon-check"></use>
                    </svg>
                   
                    <div class="recipe__ingredient">
                        ${ingredient}
                    </div>
                </li>
            `
        }
        console.log(markup);
        return markup;
    },
    renderRecipe(recipe) {
        const markup = `
            <figure class="recipe__fig">
                <img src="${recipe.image_url}" alt="${recipe.title}" class="recipe__img">
                <h1 class="recipe__title">
                    <span>${recipe.title}</span>
                </h1>
            </figure>
            <div class="recipe__details">
                <div class="recipe__info">
                    <svg class="recipe__info-icon">
                        <use href="img/icons.svg#icon-stopwatch"></use>
                    </svg>
                    <span class="recipe__info-data recipe__info-data--minutes">45</span>
                    <span class="recipe__info-text"> minutes</span>
                </div>
                <div class="recipe__info">
                    <svg class="recipe__info-icon">
                        <use href="img/icons.svg#icon-man"></use>
                    </svg>
                    <span class="recipe__info-data recipe__info-data--people">4</span>
                    <span class="recipe__info-text"> servings</span>

                    <div class="recipe__info-buttons">
                        <button class="btn-tiny">
                            <svg>
                                <use href="img/icons.svg#icon-circle-with-minus"></use>
                            </svg>
                        </button>
                        <button class="btn-tiny">
                            <svg>
                                <use href="img/icons.svg#icon-circle-with-plus"></use>
                            </svg>
                        </button>
                    </div>

                </div>
                <button class="recipe__love">
                    <svg class="header__likes">
                        <use href="img/icons.svg#icon-heart-outlined"></use>
                    </svg>
                </button>
            </div>
            <div class="recipe__ingredients">
                <ul class="recipe__ingredient-list">
                    ${this.createIngredient(recipe.ingredients)}
                </ul>

                <button class="btn-small recipe__btn">
                    <svg class="search__icon">
                        <use href="img/icons.svg#icon-shopping-cart"></use>
                    </svg>
                    <span>Add to shopping list</span>
                </button>
            </div>

            <div class="recipe__directions">
                <h2 class="heading-2">How to cook it</h2>
                <p class="recipe__directions-text">
                    This recipe was carefully designed and tested by
                    <span class="recipe__by">${recipe.publisher}</span>. Please check out directions at their website.
                </p>
                <a class="btn-small recipe__btn" href="${recipe.source_url}" target="_blank">
                    <span>Directions</span>
                    <svg class="search__icon">
                        <use href="img/icons.svg#icon-triangle-right"></use>
                    </svg>

                </a>
            </div>
        `;

        elements.recipe.insertAdjacentHTML("afterbegin", markup);
    }
};

function recipeController(e) {
    e.preventDefault();

    // Получить id
    const id = recipeView.getRecipeId(e.target);

    if (id) {
        recipeView.clearRecipe();
        loader.render(elements.recipe);
        // делаем запрос на получение одного рецепта по id
        recipeModel.getRecipe(id, function (res) {
            loader.clear();
            recipeView.renderRecipe(res.recipe);
        });
    }
}

elements.searchResList.addEventListener('click', recipeController);





// f2f_url
//     :
//     "http://food2fork.com/view/35428"
// image_url
//     :
//     "http://static.food2fork.com/Mojito2BFish2BTacos2Bwith2BStrawberry2BSalsa2B5002B732239886992.jpg"
// publisher
//     :
//     "Closet Cooking"
// publisher_url
//     :
//     "http://closetcooking.com"
// recipe_id
//     :
//     "35428"
// social_rank
//     :
//     99.99906084088893
// source_url
//     :
//     "http://www.closetcooking.com/2011/06/mojito-grilled-fish-tacos-with.html"
// title
//     :
//     "Mojito Grilled Fish Tacos with Strawberry Salsa"


// f2f_url
//     :
//     "http://food2fork.com/view/36074"
// image_url
//     :
//     "http://static.food2fork.com/codtomatoesa1300x20028e7ac34.jpg"
// publisher
//     :
//     "Simply Recipes"
// publisher_url
//     :
//     "http://simplyrecipes.com"
// recipe_id
//     :
//     "36074"
// social_rank
//     :
//     99.9989219072752
// source_url
//     :
//     "http://www.simplyrecipes.com/recipes/cod_sauteed_in_olive_oil_with_fresh_tomatoes/"
// title
//     :
//     "Cod Sauted in Olive Oil with Fresh Tomatoes"





















