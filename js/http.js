const http = {
    get(url, callback) {
        const xhr = new XMLHttpRequest();

        xhr.open("GET", url);

        xhr.addEventListener("load", function () {
            // после ответа выполним какие то действия
            callback(null, xhr.responseText);
        });

        xhr.addEventListener("error", function () {
            // в случае ошибки выполнить действия
            callback("Some error!", null);
        });

        xhr.send();
    },
    post(url, data, callback) {
        const xhr = new XMLHttpRequest();

        xhr.open("POST", url);

        xhr.addEventListener("load", function () {
            // после ответа выполним какие то действия
            callback(null, xhr.responseText);
        });

        xhr.addEventListener("error", function () {
            // в случае ошибки выполнить действия
            callback("Some error!", null);
        });

        xhr.setRequestHeader("Content-type", "application/json");

        xhr.send(JSON.stringify(data));
    },
    put(url, data, callback) {
        const xhr = new XMLHttpRequest();

        xhr.open("PUT", url);

        xhr.addEventListener("load", function () {
            // после ответа выполним какие то действия
            callback(null, xhr.responseText);
        });

        xhr.addEventListener("error", function () {
            // в случае ошибки выполнить действия
            callback("Some error!", null);
        });

        xhr.setRequestHeader("Content-type", "application/json");

        xhr.send(JSON.stringify(data));
    },
    delete(url, callback) {
        const xhr = new XMLHttpRequest();

        xhr.open("DELETE", url);

        xhr.addEventListener("load", function () {
            // после ответа выполним какие то действия
            callback(null, xhr.responseText);
        });

        xhr.addEventListener("error", function () {
            // в случае ошибки выполнить действия
            callback("Some error!", null);
        });

        xhr.send();
    }
};