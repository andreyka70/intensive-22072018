<!-- # Интсрукция по работе с GitLab.

## Для начала нужно склонировать репозиторий.

1. git clone (адрес репозитория)
В нашем случае:
```
git clone git@gitlab.com:zenway.code/implant.git
```

2. Перехожим в каталог с проектом
```
cd implant
```


## Теперь нужно создать ветку в которой будем работать

1. Для этого заходим на сайт проета на Гитлабе https://gitlab.com/zenway.code/implant
2. Жмем эту кнопку http://take.ms/R0J2o
3. Потом эту http://take.ms/w75Eq
4. В Поле Brahch Name пишем название ветки в которой будем работать. Оно должно совпадать с названием HTML файла. http://take.ms/7woUZ
5. В поле `Create from` в вывападающем меню выбираем ИЗ какой ветки будем делать. `ВСЕГДА выбирать DEV !!!`
6. Будет так: http://take.ms/23Kdk
7. Жмем `Create Brahch`

8. Теперь подтянем себе новую ветку и втерминале наберем git fetch --all
9. Увидим ее в списке http://take.ms/Edcsa
10. Теперь перейдем на нее:
```
git checkout -t origin/test
```

Работаем в ней...


## Как закончили работать (Это все нужно делать ВСЕГДА)

1. Добавим новые файлы в проект с которыми работали
```
git add .
```
2. Комитим код
```
git commit -m 'Ваш Комментарий'
```
3. Пушим код на сервер
```
git push
```

## Если ваш код ОК, все PixelPerfect, адаптивно и прошло ревью, то создаем `Merge Request`

1. После пуша кода на сервер, вы увидите в консоли такое http://take.ms/w4G5p

Перейдя по этой ссылке вы можете создать `Merge Request`

2. Вам откроется страница, на которой в поле `Assignee` вам нужно выбрать того кто будет делать ревью кода.
http://take.ms/CtqB2

3. Далее вам нужно выбрать ветку КУДА будет происходить `merge`. Нажмите `change branches` http://take.ms/T0EbF

4. В колонке `Target branch` выбрать `DEV` http://take.ms/arm66

5. Нажать эту кнопку http://take.ms/xBM13

6. Далее во вкладке `Changes` вы можете проверить все ли попало в `Merge Request` http://take.ms/9Gxyr

7. И если все ОК, то нажать `Submit merge request` http://take.ms/DNYUq

8. Все ;-)




# Zenway.code HTML App Boilerplate

#How to use

Clone this repo and then in command line type:

* `npm install` or `yarn` - install all dependencies
* `gulp` - run dev-server and let magic happen, or
* `gulp build` - build project from sources

--

## List of Gulp tasks

To run separate task type in command line `gulp [task_name]`.
Almost all tasks also have watch mode - `gulp [task_name]:watch`, but you don't need to use it directly.

### Main tasks
Task name          | Description                                                      
:------------------|:----------------------------------
`default`          | will start all tasks required by project in dev mode: initial build, watch files, run server with livereload
`build:dev`        | build dev version of project (without code optimizations)
`build`            | build production-ready project (with code optimizations)

### Other tasks
Task name          | Description                                                      
:------------------|:----------------------------------
`styles` 	         | compile .sass/.scss to .css. We also use [postcss](https://github.com/postcss/postcss) for [autoprefixer](https://github.com/postcss/autoprefixer) and [Lost](https://github.com/peterramsing/lost), so feel free to include other awesome postcss [plugins](https://github.com/postcss/postcss#plugins) when needed
`webpack`          | compile .js sources into bundle file
`copy`             | copy common files from `./app` path to `./dist` path
`svgo`             | optimize svg files with [svgo](https://github.com/svg/svgo)
`iconfont`         | compile iconfonts from svg sources
`sprite:svg`       | create svg symbol sprites ([css-tricks](https://css-tricks.com/svg-sprites-use-better-icon-fonts/))
`sprite:png`       | create png sprites
`server`           | run dev-server powered by [BrowserSync](https://www.browsersync.io/)
`clean`            | remove `./dist` folder
`ftp`              | upload `./dist` folder to specified FTP server


## Flags

We have several useful flags.

* `gulp --open` or `gulp server --open` - run dev server and then open preview in browser
* `gulp --tunnel=[name]` or `gulp server --tunnel [name]` - runs dev server and allows you to easily share a web service on your local development machine (powered by [localtunnel.me](https://localtunnel.me/)). Your local site will be available at `[name].localtunnel.me`.
* `gulp [task_name] --prod` or `gulp [task_name] --production` - run task in production mode. Some of the tasks (like, sass or js compilation) have additional settings for production mode (such as code minification), so with this flag you can force production mode. `gulp build` uses this mode by default.

## Deploy
Specify credential for FTP server into `ftp_credentials.json`

* `npm run deploy` - build project and upload `./dist` folder to specified FTP server

##Other
You can also use [npm scripts](https://docs.npmjs.com/misc/scripts):

* `npm run start` - same as `gulp default`.
* `npm run build` - same as `gulp build`.
* `npm run lint` - linting javascript with **eslint**.
* `npm run lint:fix` - fix as many issues as possible relatives to **eslint** settings. -->
